# Flask settings
#FLASK_SERVER_NAME = '146.48.96.223:9999'
FLASK_SERVER_NAME = '<iai-url>:9999'
FLASK_DEBUG = False  # Do not use debug mode in production

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# Upload mail
MAX_UPLOAD = 4

#classifier version (1: return only the spam probablity, 0: return all the information about the mail)
CLASSIFIER_VERSION = 1
