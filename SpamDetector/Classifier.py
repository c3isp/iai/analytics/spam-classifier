from Endpoints.Errors import ErrorClassifier
from SpamDetector import ClassifierConf
from keras.layers import Input, Dense, Embedding, SpatialDropout1D, concatenate, Bidirectional, GRU, Conv1D, GlobalAveragePooling1D, GlobalMaxPooling1D
from keras.models import Model
from keras import optimizers
from keras.preprocessing import sequence
from keras import backend as K

import pickle
import codecs
import numpy as np


class Classifier(object):


    def __init__(self, tokenizer, embedding_index):
        self.tokenizer = tokenizer
        self.embedding_index = embedding_index

    def loadEmbedding(self):
        #load embeddings
        print('loading word embeddings...')
        embeddings_index = {}
        f = codecs.open(ClassifierConf.EMBEDDING_PATH, encoding='utf-8')
        for line in f:
            values = line.rstrip().rsplit(' ')
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
        f.close()
        print('found %s word vectors' % len(embeddings_index))
        self.embedding_index = embeddings_index

    def createEmbeddedMatrix(self, word_index):
        embeddings_index = self.embedding_index
        words_not_found = []
        len_word_index = len(word_index)
        embedding_matrix = np.zeros((len_word_index, ClassifierConf.EMBEDDED_DIM))
        for word, i in word_index.items():
            if i >= len_word_index:
                continue
            embedding_vector = embeddings_index.get(word)
            if (embedding_vector is not None) and len(embedding_vector) > 0:
                # words not found in embedding index will be all-zeros.
                embedding_matrix[i] = embedding_vector
            else:
                words_not_found.append(word)
        return embedding_matrix

    def getEmbeddingMatrix(self):
        return self.embedding_matrix

    def createModel(self, word_index, embedding_matrix):
        #Create embedding matrix from the word index of the training
        len_word_index = len(word_index)

        #Define optimizer
        adam = optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

        inp = Input(shape=(ClassifierConf.MAX_SEQ_LEN, ))
        emb = Embedding(len_word_index, ClassifierConf.EMBEDDED_DIM, weights=[embedding_matrix], trainable = False)(inp)
        emb = SpatialDropout1D(0.2)(emb)
        x = Bidirectional(GRU(64, return_sequences=True,dropout=0.1,recurrent_dropout=0.1))(emb)
        conv1 = Conv1D(ClassifierConf.NUM_FILTERS, 7, activation='relu', padding='same', kernel_initializer = "glorot_uniform")(x)
        avg_pool = GlobalAveragePooling1D()(conv1)
        max_pool = GlobalMaxPooling1D()(conv1)
        conc = concatenate([avg_pool, max_pool])
        outp = Dense(2, activation="softmax")(conc)
        model = Model(inputs=inp, outputs=outp)
        model.compile(loss='binary_crossentropy', optimizer=adam, metrics=['accuracy'])
        return model

    def setTokenizer(self):
        with open(ClassifierConf.PATH_TOKENIZER, 'rb') as handle:
            tokenizer = pickle.load(handle)
        self.tokenizer = tokenizer


    def setClassifier(self):
        self.setTokenizer()
        tokenizer = self.tokenizer
        self.loadEmbedding()


    def prepareData(self, uploadedMails):
        mailData = []
        for mail in uploadedMails:
            mailData.append(mail.body)
        tokenizer = self.tokenizer
        word_sequence = tokenizer.texts_to_sequences(mailData)
        word_sequence = sequence.pad_sequences(word_sequence, maxlen=ClassifierConf.MAX_SEQ_LEN)
        return word_sequence

    def classify(self, uploadedMails):
        try:
            word_sequence = self.prepareData(uploadedMails)
            embedding_matrix = self.createEmbeddedMatrix(self.tokenizer.word_index)
            K.clear_session()
            model = self.createModel(self.tokenizer.word_index, embedding_matrix)
            model.load_weights(ClassifierConf.WEIGHTS)
            predictions = model.predict(word_sequence, batch_size=64, verbose=1)
            idx = 0
            for pred in predictions:
                if pred[0] > pred[1]:
                    uploadedMails[idx].setSpam(True)
                else:
                    uploadedMails[idx].setSpam(False)
                uploadedMails[idx].setSpamProbability(str(pred[0]))
                idx +=1
        except:
            errors = ErrorClassifier(2, "Fatal error in classifying mails")
            return errors

        errors = ErrorClassifier(0, "No error in classifying mails")
        return errors





