MAX_SEQ_LEN = 5000
EMBEDDED_DIM = 300
NUM_FILTERS = 64

EMBEDDING_PATH = "SpamDetector/ClassifierFiles/crawl-300d-2M.vec"
PATH_TOKENIZER = "SpamDetector/ClassifierFiles/tokenizer.pickle"
WEIGHTS = "SpamDetector/ClassifierFiles/enron_untroubled_RCNN.hdf5"