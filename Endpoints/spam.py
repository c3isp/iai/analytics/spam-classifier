from flask import Response, json
from flask_restplus import Resource, fields, marshal_with, reqparse
from werkzeug.datastructures import FileStorage
import settings
from Endpoints import Utils
from Endpoints.MailManager import extractMailFields
from Endpoints.UploadManager import readMails
from Endpoints.Utils import JsonTransformer
from restplus import api
from SpamDetector.Classifier import Classifier

ns_spam = api.namespace('spam/classifier', description='Operations related to spam classifier')
#upload_parser = api.parser()
#post_parser = api.parser()


'''
#set a max number of uploads
for upload in range(0,settings.MAX_UPLOAD):
    argument_name = "file" + str(upload)
    upload_parser.add_argument(argument_name, location='files', type=FileStorage)
'''

uri_mails = api.model('uri_mails', {
  'paths': fields.List(fields.String(required=True), description="URI MAIL", default='file:///home/user/mail/mail_sample.eml'),
})



classifier = None
# Load classifier
classifier = Classifier(None, None)
classifier.setClassifier()

@ns_spam.route("/upload", methods=['POST'])
class Spam(Resource):

    #Rest Api used to upload a file
    #@api.expect(upload_parser)
    @api.expect(uri_mails)
    def post(self):
        data_received = self.api.payload
        uris = data_received['paths']

        print("-----------UPLOAD MAILS------------")
        transformToJson = JsonTransformer()
        #Get uploaded files and create a mail array
        uploadedMails, errorUpload = readMails(uris)
        if errorUpload.status == 2 or len(uploadedMails) == 0:
            errorsJson = transformToJson.transform(errorUpload)
            return Response(errorsJson, status=400, mimetype='application/json')

        print("-----------EXTRACT FIELDS------------")
        #Extract info from the mail array and add that info to the each element
        errorExtraction, mailsExtracted = extractMailFields(uploadedMails)
        if errorExtraction.status == 2 or mailsExtracted == 0:
            errorsJson = transformToJson.transform(errorExtraction)
            return Response(errorsJson, status=400, mimetype='application/json')

        print("-----------CLASSIFY MAILS------------")
        #classify the mails uploaded
        errorClassifier = classifier.classify(uploadedMails)
        if errorClassifier.status == 2:
            errorsJson = transformToJson.transform(errorClassifier)
            return Response(errorsJson, status=400, mimetype='application/json')
        if(settings.CLASSIFIER_VERSION == 1):
            spamProbabilityResult = Utils.getSpamProbability(uploadedMails)
            classifierJsonResult = transformToJson.transform(spamProbabilityResult)
        else:
            classifierJsonResult = transformToJson.transform(uploadedMails)

        print(classifierJsonResult)
        return Response(classifierJsonResult, status=200, mimetype='application/json')

















