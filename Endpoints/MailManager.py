import mailparser
import re

from Endpoints.Errors import ErrorExtraction

try:
    from nltk import wordpunct_tokenize
    from nltk.corpus import stopwords
except ImportError:
    print('[!] You need to install nltk (http://nltk.org/index.html)')

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def remove_extra_spaces(data):
    p = re.compile(r'\s+')
    return p.sub(' ', data)


def _calculate_languages_ratios(text):
    """
    Calculate probability of given text to be written in several languages and
    return a dictionary that looks like {'french': 2, 'spanish': 4, 'english': 0}

    @param text: Text whose language want to be detected
    @type text: str

    @return: Dictionary with languages and unique stopwords seen in analyzed text
    @rtype: dict
    """
    languages_ratios = {}
    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]

    # Compute per language included in nltk number of unique stopwords appearing in analyzed text
    for language in stopwords.fileids():
        stopwords_set = set(stopwords.words(language))
        words_set = set(words)
        common_elements = words_set.intersection(stopwords_set)
        languages_ratios[language] = len(common_elements)  # language "score"
    return languages_ratios


def detect_language(text):
    """
    Calculate probability of given text to be written in several languages and
    return the highest scored.

    It uses a stopwords based approach, counting how many unique stopwords
    are seen in analyzed text.

    @param text: Text whose language want to be detected
    @type text: str

    @return: Most scored language guessed
    @rtype: str
    """
    ratios = _calculate_languages_ratios(text)
    most_rated_language = max(ratios, key=ratios.get)
    return most_rated_language


#Function that, given an array of mails, extract the most important fields
def extractMailFields(mails):
    discarded_mails = 0
    accepted_mails = 0

    try:
        for mail in mails:
            mailparsered = mailparser.parse_from_string(mail.original_mail)

            #extract mail subject
            subject = mailparsered.subject

            #extract mail body
            body = mailparsered.body
            body_cleaned = cleanhtml(str(body))
            body_cleaned = remove_extra_spaces(body_cleaned)

            #extract language
            body_subject = str(subject) + "\n" +  body_cleaned
            language = detect_language(str(body_subject))

            #extract sender
            sender = mailparsered.from_
            if (len(sender) == 0):
                sender = ""

            #extract receiver
            receiver = mailparsered.to
            if (len(receiver) == 0):
                receiver = ""

            if(body_cleaned == "" or language != "english"):
                discarded_mails +=1
                continue

            accepted_mails +=1
            mail.setSubject(str(subject))
            mail.setBody(body_cleaned)
            mail.setLanguage(language)
            mail.setReceiver(receiver)
            mail.setSender(sender)

    except:
        errors = ErrorExtraction(2, "Fatal error in extracting data from mails")
        return errors

    if discarded_mails == 0:
        errors = ErrorExtraction(0, "No error in extracting data from mails")
    else:
        errors = ErrorExtraction(1, "No error in extracting data from mails. " + str(accepted_mails) + "/" + str(discarded_mails + accepted_mails) + " extracted. (Some body mails could be empty or couldn't be in english)")

    return errors, accepted_mails






