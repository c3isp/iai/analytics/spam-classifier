import json


class Mail:

    def __init__(self, filename, subject, body, language, spam, sender, receiver, original_mail, spamProbability):
        self.filename = filename
        self.subject = subject
        self.body = body
        self.language = language
        self.isSpam = spam
        self.spamProbability = spamProbability
        self.sender = sender
        self.receiver = receiver
        self.original_mail = original_mail

    def setFilename(self, filename):
        self.filename = filename

    def setSpamProbability(self, spamProbability):
        self.spamProbability = spamProbability

    def setSubject(self, subject):
        self.subject = subject

    def setMailOriginal(self,mail_original):
        self.mail_original = mail_original

    def setBody(self, body):
        self.body= body

    def setLanguage(self, language):
        self.language = language

    def setSpam(self, spam):
        self.isSpam = spam

    def setSender(self, sender):
        self.sender = sender

    def setReceiver(self, receiver):
        self.receiver = receiver

    def getName(self):
        return self.name

    #def toJSON(self):
    #    return json.dumps(self, default=lambda o: o.__dict__,
    #                      sort_keys=True, indent=4)



