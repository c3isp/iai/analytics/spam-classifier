import jsonpickle

class JsonTransformer(object):
    def transform(self, myObject):
        return jsonpickle.encode(myObject, unpicklable=False)


def getSpamProbability(uploadedMails):
    result = []

    for mail in uploadedMails:
        mail_res = {}
        mail_res["filename"] = mail.filename
        mail_res["spamProbability"] = mail.spamProbability
        '''
        mail_res["subject"] = mail.subject
        sender_string = ""
        for sender in mail.sender[0]:
                if sender != "":
                        sender_string = sender_string + " " + sender

        receiver_string = ""
        for receiver in mail.receiver[0]:
                if receiver != "":
                        receiver_string = receiver_string + ", " + receiver
        mail_res["source"] = sender_string
        mail_res["destination"] = receiver_string
        '''
        result.append(mail_res)

    return result
