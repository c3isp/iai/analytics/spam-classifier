import urllib

import settings
import pathlib
import urllib.parse, urllib

#Function that cheks if the file path respect the extensions permitted
from Endpoints.Errors import ErrorUpload
from Endpoints.Mail import Mail


def checkextension(pathfile):
    extensionPermitted = [".eml", ".txt", ".dpo"]
    extension = pathlib.Path(pathfile).suffix
    if extension in extensionPermitted:
        return True
    else:
        return False
'''
#Function that manages the uploading files
def getuploadedfiles(upload_parser):
    errorextension = 0
    correctlyreceived = 0
    receivedMail = []
    args = upload_parser.parse_args()
    try:
        for upload in range(0, settings.MAX_UPLOAD):
            argument_name = "file" + str(upload)
            uploaded_file = args[argument_name]  # This is FileStorage instance

            if uploaded_file != None:
                if(checkextension(uploaded_file.filename)):
                    mail = Mail(uploaded_file.filename, "", "", "", "", "", "", uploaded_file.read().decode('utf-8').replace('"', ''), None)
                    #mail = Mail(uploaded_file.filename, "", "", "", "", "", "",uploaded_file.read().decode('ISO-8859-1').replace('"', ''), None)
                    receivedMail.append(mail)
                    correctlyreceived += 1
                else:
                    errorextension +=1
    except:
        errors = ErrorUpload(2, "Fatal error in receiving data")
        return receivedMail, errors

    if errorextension == 0:
        errors = ErrorUpload(0, "No errors in receiving data")
    else:
        errors = ErrorUpload(1, "Error in receiving data. " + str(correctlyreceived) + "/" + str(correctlyreceived + errorextension))


    return receivedMail, errors
'''

#Function that read the mails given a list of uri (file:///home/giacomo/mail.eml)
def readMails(uri_mails):
    errorextension = 0
    correctlyreceived = 0
    receivedMail = []
    try:
        for uri in uri_mails:
            print("Reading " + uri)
            from urllib.parse import urlparse
            path_mail = urlparse(uri)[2]
            print(path_mail)
            if (checkextension(path_mail)):
                mail_content = ""
                with open(path_mail, 'r') as mail_file:
                    mail_content = mail_file.read().replace('"', '')
                mail = Mail(path_mail, "", "", "", "", "", "", mail_content, None)
                receivedMail.append(mail)
                correctlyreceived += 1
            else:
                errorextension +=1


    except:
        errors = ErrorUpload(2, "Fatal error in receiving data")
        return receivedMail, errors

    if errorextension == 0:
        errors = ErrorUpload(0, "No errors in receiving data")
    else:
        errors = ErrorUpload(1, "Error in receiving data. " + str(correctlyreceived) + "/" + str(correctlyreceived + errorextension))


    return receivedMail, errors



